#!/bin/bash
a=$(diff /home/djon-con/bitbucket/scripts/new /home/djon-con/bitbucket/scripts/old)
if [ -n "$a" ]
then
	echo "CRON HAS BEEN CHANGED" | mail -s "CRONTAB CHANGED" root
	mv new old
	shasum -a 256 /var/spool/cron/crontabs/root > new
fi
